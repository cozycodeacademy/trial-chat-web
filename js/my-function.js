// Ex.1
// function provided
//
// app.createChannel(channelName)
// create a new channel with a name
//
// app.clearChannelInput()
// clear the channel name input box

function handleNewChannelCreation(channelName) {
  // TODO
  // when new channel button is clicked, you need to do
  // 1. check the channel name first, if the channel name is not empty string "", create the channel
  // 2. clear the input box

}


// Ex.2
// function provided
//
// app.updateChannelList(messageChannel, messageContent, messageTime)
// update message and time on the channel list
//
// app.refreshChannelOrder()
// reorder the channel list by last message time
//
// app.createMessage(messageContent, messageTime, sentByMe)
// create and add a message to the current conversation box
//
// app.chatScrollToBottom()
// scroll to the bottom of current conversation box

function handleNewMessage(currentChannel, messageChannel, messageContent, messageTime, sentByMe) {
  // TODO
  // when a new message is sent/arrived, you need to do
  // 1. update the message on the channel list
  // 2. refresh the channel list order
  // 3. if the current channel equals to the channel of new message, add new message to the conversation box, scroll to bottom

}

