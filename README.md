# 編寫實時通訊程式體驗 Cozy Code Academy
[瀏覽官方網頁了解詳情](https://cozycode.co/courses/free-trial)
# 如何使用
1. [下載程式碼](https://gitlab.com/cozycodeacademy/trial-chat-web)
2. 使用瀏覽器打開chat-demo.html試用製成品
3. 使用Microsoft Visual Code打開class-exercise.html學習JavaScript
4. 使用Microsoft Visual Code打開chat-trial.html開始編寫程式
5. 按chat-trial.html內提供的function完成網頁
 
# Chat Web Trial Class - Cozy Code Academy
[Learn more the official website](https://cozycode.co/courses/free-trial)
# How to use
1. [Download the project](https://gitlab.com/cozycodeacademy/trial-chat-web)
2. Use browser to open chat-demo.html and try the feature
3. Use Microsoft Visual Code to open class-exercise.html to learn JavaScript
4. Use Microsoft Visual Code to open chat-trial.html, start your coding
5. Finish the chat-trial.html using the functions provided in HTML file
